package ru.mai.dep810.spring;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.HazelcastInstance;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import org.springframework.stereotype.Component;
import ru.mai.dep810.spring.model.Pet;

@Component
public class HzPetClinicServiceImpl implements PetClinicService {

    private PetClinicService delegate;
    private HazelcastInstance client =
            HazelcastClient.newHazelcastClient();

    private Map<Long, Pet> cache = client.getMap("pets");

    private final Lock lock = client.getLock("petLock");

    public HzPetClinicServiceImpl(PetClinicServiceImpl delegate) {
        this.delegate = delegate;
        client.getTopic("petUpdateTopic").addMessageListener(
                message ->System.out.println("Message received: " + message.getMessageObject())
        );
    }

    @Override
    public Pet register(Pet pet) {
        Pet savedPet = delegate.register(pet);
        cache.put(savedPet.getId(), savedPet);
        client.getTopic("petUpdateTopic").publish(savedPet.getId());
        return savedPet;
    }

    @Override
    public Collection<Pet> findAllPets() {
        return delegate.findAllPets();
    }

    @Override
    public Pet findPetById(Long id) {
        return cache
                .computeIfAbsent(id, (key) -> delegate.findPetById(key));
    }

    @Override
    public void unregister(Long id) {
        lock.lock();
        try {
            cache.remove(id);
            delegate.unregister(id);
        } finally {
            lock.unlock();
        }
    }

    public void clearCache() {
        cache.clear();
    }
}
