package ru.mai.dep810.spring;

import com.google.common.cache.CacheStats;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mai.dep810.spring.model.Pet;

@RestController
public class PetController {

    @Autowired
//    private CachedPetClinicServiceImpl petClinicService;
    private HzPetClinicServiceImpl petClinicService;

    @PostMapping("/pet")
    public Pet register(@RequestBody Pet pet) {
        return petClinicService.register(pet);
    }

    @RequestMapping("/pet")
    public Collection<Pet> listAllPets() {
        return petClinicService.findAllPets();
    }

    @GetMapping(value = "/pet/{id}")
    public Pet getById(@PathVariable Long id) {
        return petClinicService.findPetById(id);
    }

    @DeleteMapping(value = "/pet/{id}")
    public void unregister(@PathVariable Long id) {
        petClinicService.unregister(id);
    }

//    @GetMapping("/cache/stats")
//    public CacheStats cacheStats() {
//        return petClinicService.getStats();
//    }
}
