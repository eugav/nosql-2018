package ru.mai.dep810.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(DatabaseConfiguration.class)
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

//    @Bean
//    public MailService mailService() {
//        return new MailService();
//    }

//    @Bean
//    public MongoOperations mongoOperations() {
//        return new MongoTemplate(
//                new MongoClient("localhost", 27017),
//                "stud"
//        );
//    }
}
