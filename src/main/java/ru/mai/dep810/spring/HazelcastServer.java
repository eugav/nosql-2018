package ru.mai.dep810.spring;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.eviction.LRUEvictionPolicy;

public class HazelcastServer {

    public static void main(String[] args) {
        Config config = new Config();

        MapConfig mapConfig = new MapConfig("pets")
                .setMaxSizeConfig(
                        new MaxSizeConfig(10, MaxSizeConfig.MaxSizePolicy.PER_NODE)
                )
                .setMapEvictionPolicy(new LRUEvictionPolicy());
//        mapConfig.setMapStoreConfig(
//                new MapStoreConfig().setImplementation(new MyMapStore())
//        );
        config.addMapConfig(mapConfig);
        HazelcastInstance instance = Hazelcast
                .newHazelcastInstance(config);
    }
}
