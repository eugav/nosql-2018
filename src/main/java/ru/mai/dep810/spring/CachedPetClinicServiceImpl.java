package ru.mai.dep810.spring;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheStats;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import org.springframework.stereotype.Component;
import ru.mai.dep810.spring.model.Pet;

@Component
public class CachedPetClinicServiceImpl implements PetClinicService {

    private PetClinicService delegate;
    private Cache<Long, Pet> cache = CacheBuilder.newBuilder()
            .maximumSize(10)
            .expireAfterAccess(60, TimeUnit.SECONDS)
            .recordStats()
            .build();

    public CachedPetClinicServiceImpl(PetClinicServiceImpl delegate) {
        this.delegate = delegate;
    }

    @Override
    public Pet register(Pet pet) {
        Pet savedPet = delegate.register(pet);
        cache.put(savedPet.getId(), savedPet);
        return savedPet;
    }

    @Override
    public Collection<Pet> findAllPets() {
        return delegate.findAllPets();
    }

    @Override
    public Pet findPetById(Long id) {
        return cache.asMap()
                .computeIfAbsent(id, (key) -> delegate.findPetById(key));
    }

    @Override
    public void unregister(Long id) {
        cache.asMap().remove(id);
        delegate.unregister(id);
    }

    public void clearCache() {
        cache.asMap().clear();
    }

    public CacheStats getStats() {
        return cache.stats();
    }
}
