package ru.mai.dep810.spring;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

import static java.util.Collections.singletonList;

@Configuration
public class DatabaseConfiguration extends AbstractMongoConfiguration {

    private static final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);

    @Value("${mongodb.addresses}")
    private String addresses;

    @Override
    public String getDatabaseName() {
        return "stud";
    }

    @Override
    @Bean
    public MongoClient mongoClient() {
        return new MongoClient(parseAddresses(addresses));
    }

    private List<ServerAddress> parseAddresses(String transportAddresses) {
        return Arrays.stream(transportAddresses.split("[,;]"))
                .map(this::parseAddress)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private ServerAddress parseAddress(String address) {
        String[] hostAndPort = address.split(":");
        if (hostAndPort.length < 2 || hostAndPort.length > 2) {
            log.error("Invalid address: " + address + ". Expected 'hostname:port'");
            return null;
        }
        String host = hostAndPort[0].trim();
        Integer port = Integer.parseInt(hostAndPort[1].trim());
        log.info("Configuring mongodb with node " + host + ":" + port);
        return new ServerAddress(hostAndPort[0], Integer.parseInt(hostAndPort[1]));
    }

}