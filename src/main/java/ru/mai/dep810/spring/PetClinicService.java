package ru.mai.dep810.spring;

import java.util.Collection;
import ru.mai.dep810.spring.model.Pet;

public interface PetClinicService {
    Pet register(Pet pet);

    Collection<Pet> findAllPets();

    Pet findPetById(Long id);

    void unregister(Long id);
}
