package ru.mai.dep810.spring;

import org.elasticsearch.action.ListenableActionFuture;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
public class ElasticRepository {

    @Autowired
    Client client;

    @Value("${elastic.index.name}")
    String indexName;

    public SearchResult<Map<String, Object>> findPosts(String query) {

        ListenableActionFuture<SearchResponse> execute = client.prepareSearch(indexName)
                .setQuery(QueryBuilders.queryStringQuery(query))
                .setFetchSource(new String[] {"DisplayName", "Reputation"}, new String[] {})
                .execute();

        return convertToPosts(execute);
    }

    public SearchResult<Map<String, Object>> findPostsByField(
            String field,
            String fieldValue,
            boolean fuzzy,
            int start,
            int limit
    ) {
        QueryBuilders.boolQuery()
                .must(
                        QueryBuilders.boolQuery().should(
                                QueryBuilders.termQuery("DisplayName", "java")
                        ).should(QueryBuilders.rangeQuery("Reputation").gt(100)));
        ListenableActionFuture<SearchResponse> execute = client.prepareSearch(indexName)
                .setQuery(fuzzy ? QueryBuilders.fuzzyQuery(field, fieldValue) : QueryBuilders.termQuery(field, fieldValue))
                .setFrom(start)
                .setSize(limit)
                .setFetchSource(new String[] {"DisplayName", "Reputation"}, new String[] {})
                .addSort(SortBuilders.fieldSort("DisplayName"))
                .execute();

        return convertToPosts(execute);
    }

    private SearchResult<Map<String, Object>> convertToPosts(ListenableActionFuture<SearchResponse> execute) {
        try {
            SearchResponse searchResponse = execute.get();
            List<Map<String, Object>> rows = Arrays.stream(searchResponse.getHits().getHits())
                    .map(SearchHit::getSourceAsMap)
                    .collect(Collectors.toList());
            return new SearchResult<>(rows, searchResponse.getHits().getTotalHits());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}