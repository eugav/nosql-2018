package ru.mai.dep810.spring;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PostsController {

    @Autowired
    ElasticRepository elasticRepository;

    @RequestMapping("/posts")
    @ResponseBody
    public SearchResult<Map<String, Object>> searchPosts(
            @RequestParam("q") String query
    ) {
        return elasticRepository.findPosts(query);
    }


}
