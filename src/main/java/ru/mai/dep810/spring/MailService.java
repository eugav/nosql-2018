package ru.mai.dep810.spring;

import org.springframework.stereotype.Component;

@Component
public class MailService {

    public void sendMail(String email, String text) {
        System.out.println("Email sent to " + email);
    }

}
