package ru.mai.dep810.spring;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;
import ru.mai.dep810.spring.model.Pet;

@Component
public class PetClinicServiceImpl implements PetClinicService {

    @Autowired
    private MongoOperations mongoOps;

    private AtomicLong sequence = new AtomicLong(0);

    @Override
    public Pet register(Pet pet) {
        pet.setId(sequence.getAndIncrement());
        mongoOps.insert(pet);
        return pet;
    }

    @Override
    public Collection<Pet> findAllPets() {
        return mongoOps.findAll(Pet.class, "pet");
    }

    @Override
    public Pet findPetById(Long id) {
        return mongoOps.findById(id, Pet.class, "pet");
    }

    @Override
    public void unregister(Long id) {
        Pet pet = new Pet();
        pet.setId(id);
        mongoOps.remove(pet);
    }
}
